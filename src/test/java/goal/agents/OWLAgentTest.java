package goal.agents;

import static org.junit.Assert.assertEquals;

public class OWLAgentTest extends ProgramTest {
	// @Test
	public void owlAgentTest() throws Exception {
		assertEquals("<http://www.example.org/family#Susan>",
				runAgent("src/test/resources/goal/agents/owltest.mas2g", "bel( :Female(?x) )"));
	}
}
