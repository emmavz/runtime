/**
 * The GOAL Runtime Environment. Copyright (C) 2015 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package goal.agents;

import static org.junit.Assert.assertSame;

import java.util.Map;

import org.junit.Test;

public class SendProgramTest extends ProgramTest {
	@Test
	public void sendWithVariableTest() throws Exception {
		assertSame(RunResult.OK,
				runAgent("src/test/resources/goal/core/program/sendWithVariable/sendWithVariable.mas2g"));
	}

	@Test
	public void sendWithChannelTest() throws Exception {
		assertSame(RunResult.OK,
				runAgent("src/test/resources/goal/core/program/sendWithChannel/sendWithChannel.mas2g"));
	}

	@Test
	public void tarzanJaneCommunicationTest() throws Exception {
		// tarzan&jane does not terminate, we need to use a deadline.
		Map<String, RunResult> results = runAgents("src/test/resources/goal/core/program/TarzanAndJane/meeting.mas2g",
				1);
		assertSame(RunResult.OK, results.get("tarzan"));
		assertSame(RunResult.OK, results.get("jane"));
	}
}