% indicates we posess token (Id,N) 
% where Id is the token Id and N the remaining number of rounds to travel.
% if N=0 the token has to be forwarded to the leader, otherwise to the next in the ring.
:- dynamic(token/2).
% next(AgentId) where AgentId is the name of the next agent to send the token to.
:- dynamic(next/1).
% the leader will inform us who he is.
:- dynamic(leader/1).
% inserted when we are done.
:- dynamic(finished/0). 


% Get token that needs to be sent and agent who needs to receive the token.
% the new token uses the NewN which is N decreased by one.
% Last token (N=0) needs to be sent to leader, all other tokens are sent to next agent in ring.
message(Id, 0, Receiver,0) :- !,leader(Receiver).
message(Id, N, Receiver, NewN) :- next(Receiver), NewN is N-1.

