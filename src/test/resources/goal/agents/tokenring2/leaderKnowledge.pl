%start time. Set from init.
:- dynamic(startTime/1).

% HACK: some variable shared between init and submodule code,
% used to initialize the ring. This is a trick used to 
% remember the previous agent when running a foreach loop in leaderInit.
:-dynamic(nodeToConnectTo/1).

% HACK : some variable shared between distributeTokens and SendTokens,
% keeping track of the remaining number of tokens to send.
:- dynamic(nToSend/1).


% Number of times that a token will be forwarded.
times(1000).
% Number of tokens.
tokens(50). %50
%nr of agents (excluding the leader) in the MAS 
nrOfAgents(100). %100
% the base name of the agents in the MAS
agentBasename('ag').


% agentName(+N, -AgentName) the name of agent N. N in [0..nrOfAgents-1]
% first agent is just the basename.
% This function only works properly in GOAL the first time you run the MAS.
agentName(0,AgentName) :- !,agentBasename(AgentName).
% the other agents have extension _X with X being the number minus 1.
agentName(N,AgentName) :- agentBasename(B), N1 is N-1, 
	atom_concat(B,'_',Temp), atom_concat(Temp,N1,AgentName).

% true if +-AgentName is an actual agent name. Uses the agentName predicate
agent(AgentName) :- nrOfAgents(NA), MaxNr is NA-1, numlist(0,MaxNr,AgentNumbers), member(Nr, AgentNumbers), agentName(Nr, AgentName).
	
% -AgentList containing all agent names.
agents(AgentList):- findall(X, agent(X), AgentList).
	
% return seconds with two decimals (and not more to avoid suggesting ridiculous precision).
timeUsed(Seconds) :- startTime(StartTime), get_time(Now), Duration is Now-StartTime,
	stamp_date_time(Duration, date(_, _, D, H, Mn, S, _, _, _), 'UTC'), Seconds is round((H*3600+Mn*60+S)*100)/100.

